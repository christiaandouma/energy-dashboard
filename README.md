# Energy Dashboard

Public repo to go with an [article on CleverCrib.net on monitoring energy usage](https://www.clevercrib.net/visualise-energy-consumption-with-grafana/) using realtime data from smart energy meters. In this setup [Home Assistant (HA)](https://www.home-assistant.io/) is used to read the data from the smart energy meter. HA stores its data in [InfluxDB](https://www.influxdata.com/) and [Grafana](https://grafana.com/) is used to visualise the data. 

This repo shares the code to make this dashboard:
![Example Grafana Energy Dashboard](http://www.clevercrib.net/wp-content/uploads/2019/01/Schermafbeelding-2019-01-11-om-20.25.33-e1547471293849.png)